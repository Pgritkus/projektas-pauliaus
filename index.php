<!DOCTYPE html>
<html>
<head>
	<title>Title</title>
	<?php include 'headerlink.html'; ?>
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<script src="js/script.js"></script>
	<?php 
		$dir          = 'headimg';
		$ImagesA = Get_ImagesToFolder($dir);
		//print_r($ImagesA);

		function Get_ImagesToFolder($dir){
			$ImagesArray = [];
			$file_display = [ 'jpg', 'jpeg', 'png', 'gif' ];
			

			if (file_exists($dir) == false) {
				return ["Directory \'', $dir, '\' not found!"];
			} 
			else {
				$dir_contents = scandir($dir);
				foreach ($dir_contents as $file) {
					$file_type = pathinfo($file, PATHINFO_EXTENSION);
					if (in_array($file_type, $file_display) == true) {
						$ImagesArray[] = $file;
						
					}
				}
				
				return $ImagesArray;
			}
		}


		?>
		<script type="text/javascript">
			 
           var images = [], x = 0;
           <?php 
           for($i=0;$i<count($ImagesA);$i++)
           {
          echo "images[$i] = \"$dir/$ImagesA[$i]\";";
          echo "\r\n";
        }
        
        ?>
         function displayNextImage() {
         
              x = (x === images.length - 1) ? 0 : x + 1;
        	document.getElementById("first").style.opacity = 0;
              console.log(document.getElementById('first').src);
              document.getElementById("first").src = images[x];
             document.getElementById("first").style.opacity = 1;
          }
           function startTimer() {
              setInterval(displayNextImage, 4000);
              console.log('start timer');
          }

		</script>
</head>
<body onload = "startTimer()" >

<?php 
include 'header.html';

 ?>
	
	<main >

		<div class="parallax-container">
		<h2 class="center-align white-text truputis">Truputis apie moto</h2>
		
      	<div class="parallax"><img class="responsive-img " id="first" src='<?php echo "$dir/$ImagesA[0]" ;  ?>  '></div>
      	
    	</div>
        <ul class="row">
        	<li class=" center-align col s12 l4">
        		<img class="small" src="images/standard.jpg">
        		<h5 class="valign center-align">Standartiniai</h5>
        		<a href="type.php?tipas=standart" class="waves-effect waves-light btn z-depth-4">Eiti</a>
        		<p>Daugiau apie Standard <a href="https://en.wikipedia.org/wiki/Types_of_motorcycles#Standard">Wiki</a></p>
        		<p><a href=""></a></p>
        	</li>
        	<li class="center-align col s12 l4">
        		<img class="small" src="images/sportbike.jpg">
        		<h5 class="valign center-align">Sportiniai</h5>
        		<a href="type.php?tipas=sport" class="waves-effect waves-light btn z-depth-4">Eiti</a>
        		<p>Daugiau apie Sport bike <a href="https://en.wikipedia.org/wiki/Types_of_motorcycles#Sport_bike">Wiki</a></p>
        		<p><a href=""></a></p>
        	</li>
        	<li class="center-align col s12 l4">
        		<img class="small" src="images/cruzier.jpg">
        		<h5 class="valign center-align">Touring'ai</h5>
        		<a href="type.php?tipas=touring" class="waves-effect waves-light btn z-depth-4">Eiti</a>
        		<p>Daugiau apie Touring <a href="https://en.wikipedia.org/wiki/Types_of_motorcycles#Touring">Wiki</a></p>
        		<p><a href=""></a></p>
        	</li>
        </ul>
    </main>	
    <footer>
    	<?php 
    		include 'footer.html';
    	 ?>

    </footer>
</body>
</html>